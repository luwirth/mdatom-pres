auto MDRun::performStep(
  std::vector<double> &positions,
  std::vector<double> &velocities,
  int nstep,
  double time
) -> void {
  /* omitted */

  if (par.pressureCouplingType == PressureCouplingType::Weak) {

  // compute scale factor mu
    double mu = std::pow(
      1.0 +
        (par.isothermalCompressibility * par.timeStep)
        / par.pressureCouplingTime * (pres - par.targetPressure),
      1.0 / 3.0,
    );

    // rescale box size
    par.boxSize[0] *= mu;
    par.boxSize[1] *= mu;
    par.boxSize[2] *= mu;
    vol = par.boxSize[0] * par.boxSize[1] * par.boxSize[2];

    // rescale particle positions
    for (int j3 = 0; j3 < nat3; j3++)
      positions[j3] *= mu;
  }
  /* omitted */
}
