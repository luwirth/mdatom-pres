auto MDRun::run(
  std::vector<double> &x,
  std::vector<double> &v
) -> void {
  /* omitted */

  double timeUntilNextCollision = Rng::exponential(
    1.0 / par.meanTimeBetweenCollision
  );
  double collisionTime = time + timeUntilNextCollision;

  for (int nstep = 0; nstep < par.numberMDSteps; nstep++) {
    performStep(x, v, nstep, time);
    time += par.timeStep;

    if (par.temperatureCouplingType == TemperatureCouplingType::Strong) {

      while (collisionTime < time) {
        unsigned icollidingAtom = Rng::uniform_discrete(par.numberAtoms - 1);

        double sd = sqrt(boltzmann * par.targetTemperature / par.atomicMass);
        for (int icoord = 0; icoord < 3; ++icoord) {
          v[3 * icollidingAtom + i] = Rng::gauss(0.0, sd);
        }

        timeUntilNextCollision = Rng::exponential(
          1.0 / par.meanTimeBetweenCollisions
        );
        collisionTime += timeUntilNextCollision;
      }
    }
  }
  /* omitted */
}
