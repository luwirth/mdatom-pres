#let deriv(f, x) = $(dif#f)/(dif#x)$
#let deriv2(f, x) = $(dif^2#f)/(dif#x^2)$
#let derivpart(f, x) = $(diff#f)/(diff#x)$

#let avec(a) = math.upright(math.bold(a))
#let amat(a) = math.upright(math.bold(a))

#let vvec(a) = math.accent(math.bold(a), math.arrow)
#let nvec(a) = math.accent(avec(a), math.hat)

#let xv = $avec(x)$

#let linf(a) = $cal(#a)$
#let bilf(a) = $sans(#a)$

#let grad = $avec("grad")$

#let transp = math.tack.b
#let restr(a) = $lr(#a|)$

#let openint(a,b) = $lr(\] #a, #b \[)$

#let myblock(color: red.darken(40%), width: 100%, title, content) = block(
  fill: color,
  stroke: white,
  inset: 0.3em,
  radius: 0.3em,
  breakable: false,
  [
    #block(inset: 0.2em)[#pad(x: 1.0em)[*#title*]]
    #block(
      fill: black.lighten(10%),
      inset: 1.2em,
      above: 0.3em,
      width: width,
      [
        #content
      ]
    )
  ]
)

#let circletext(content) = box(
  baseline: 0.2em,
  stroke: white,
  inset: 0.2em,
  radius: 0.3em,
  content,
)

#let math-template(doc) = [
  #set math.mat(delim: "[")
  #set math.vec(delim: "[")
  #set math.cancel(stroke: red)
  #show math.equation: set text(black)

  #doc
]

#let lwirth-template(doc) = [
  #show: math-template

  #set text(black)
  #set page(fill: white)
  #set page(margin: 2cm)
  #set par(justify: true)

  #doc
]