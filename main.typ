#import "lwirth.typ": *

#show: lwirth-template
#set page(paper: "presentation-16-9", margin: 1.0cm)

#[
  #set text(14pt)

  = MD with coupling to a temperature or pressure bath
  #v(1cm)

  #grid(
    columns: (1fr, 1fr),
    gutter: 1.5cm,
    [
      === Strong temperature bath coupling

      - Step 1:
        Determine _time until next stochastic collision_ $Delta t_"coll"$
        based on _exponential distribution_ parametrized by
        _mean time between collisions_ $tau$.
        $ Delta t_"coll" tilde.op exp(lambda = 1/tau) $
        Run classical MD simulation until then.
      - Step 2:
        Uniformly choose random atom $i in {1, dots.h, N}$ which is experiencing collision.
        $ i tilde.op cal(U){1, dots.h, N} $
      - Step 3:
        Set velocity components of atom $i$ based on _Maxwellian velocity distribution_,
        which is parametrized by
        _target temperature_ $T_t$.
        $ v_(i,x), v_(i,y), v_(i,z) tilde.op cal(N)(mu = 0, sigma = sqrt((k_B T_t)/m)) $
      Repeat procedure.
    ],
    [
      === Weak pressure bath coupling

      In every MD step scale all sides of the system and all coordinates of the particles by
      scaling factor $mu$.
      $
        x' = mu x \
        y' = mu y \
        z' = mu z \
      $
      with
      $
        mu = (1 + (beta_T Delta t)/tau (P(t_n) - P_t))
      $
      where
      - $P_t$ is the target pressure,
      - $P(t_n)$ is the current temperature at time step $n$,
      - $tau$ is the pressure coupling time constant,
      - $beta_T$ is the isothermal compressibility (chosen as $beta_T ident 1$)

    ],
  )
]

#pagebreak()

#[
  #set text(10pt)

  #align(horizon, grid(
    columns: (1fr, 1fr),
    gutter: 1.5cm,
    [
      === Strong temperature bath coupling
      #v(0.2cm)
      #raw(read("res/code/temperature_coupling.cpp"), lang: "cpp")
    ],
    [
      === Weak pressure bath coupling
      #v(0.2cm)
      #raw(read("res/code/pressure_coupling.cpp"), lang: "cpp")
    ],
  ))
]

#pagebreak()

#[
  #set page(margin: 0.3cm)
  #table(
    columns: (2.3fr, 1fr),
    inset: 2pt,
    align: center + horizon,
    table(
      rows: (2fr, 1fr),
      inset: 0pt,
      align: center + horizon,
      stroke: none,
      image("res/img/thermo1.svg"),
      table(
        columns: (1fr, 1fr, 1fr),
        inset: 0pt,
        align: center + horizon,
        stroke: none,
        image("res/img/thermo2-120.svg"), 
        image("res/img/thermo2-100.svg"),
        image("res/img/thermo2-200.svg"),
      ),
    ),
    table(
      rows: (1fr, 1fr, 1fr),
      inset: 0pt,
      align: center + horizon,
      stroke: none,
      image("res/img/comp-energy.svg"), 
      image("res/img/comp-temperature.svg"), 
      image("res/img/comp-pressure.svg")
    )
  )
]